# RH Digital



## Sobre o projeto:

Projeto está sendo desenvolvido com o intuito de administrar lançamentos de vagas e candidaturas, assim facilitando a visualização e a organização.

## Desenvolvimento:

```
Preparação de ambiente virtual:
$ pip install virtualenv
$ python -m venv venv
```

```
Preparando as ferramentas(necessário ter o python instalado):
$ pip install django
$ pip install django-rest_framework
```
# Pode ser necessário criar um super usuario para o admin, neste caso use o comando:
```
Para rodar o projeto:
$ python manage.py runserver
Para criar o super usuario:
$ python manage.py createsuperuser
```