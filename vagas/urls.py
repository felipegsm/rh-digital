from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static

from . import views
urlpatterns=[
    path('',views.home,name='home'),
    path('todas_vagas',views.todas_vagas,name='todas_vagas'),
    path('detalhes/<int:id>',views.detalhes,name='detalhes'),
    path('totas_categorias',views.todas_categorias,name='todas_categorias'),
    path('categoria/<int:id>',views.categoria,name='categoria'),
    path('login', include('login.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)