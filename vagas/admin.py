from django.contrib import admin

from .models import Categoria, Vagas, Candidatura

# Register your models here.

admin.site.register(Categoria)

class AdminVagas(admin.ModelAdmin):
    list_display=('titulo','categoria','hora_postagem')

admin.site.register(Vagas,AdminVagas)

class AdminComment(admin.ModelAdmin):
    list_display=('vaga', 'nome', 'email', 'numero', 'escolaridade', 'experiencia', 'status')
admin.site.register(Candidatura,AdminComment)