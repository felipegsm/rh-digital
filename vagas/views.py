from django.shortcuts import render
from django.contrib import messages
from .models import Vagas, Categoria, Candidatura

# Pagina inicial
def home(request):
    primeira_vaga=Vagas.objects.all()
    terceira_categoria=Categoria.objects.all()[0:3]
    return render(request,'home.html',{
        'primeira_vaga':primeira_vaga,
        'terceira_categoria':terceira_categoria
    })

def detalhes(request,id):
    vaga=Vagas.objects.get(pk=id)
    if request.method=='POST':
        nome=request.POST['nome']
        email=request.POST['email']
        numero=request.POST['numero']
        escolaridade=request.POST['escolaridade']
        experiencia=request.POST['experiencia']
        Candidatura.objects.create(
            vaga=vaga,
            nome=nome,
            email=email,
            numero=numero,
            escolaridade=escolaridade,
            experiencia=experiencia
        )
        messages.success(request,'Candidatura enviada com sucesso. Boa sorte.')
    categoria=Categoria.objects.get(id=vaga.categoria.id)
    rel_vagas=Vagas.objects.filter(categoria=categoria).exclude(id=id)
    candidatura=Candidatura.objects.filter(vaga=vaga,status=True).order_by('-id')
    return render(request,'detalhes.html',{
        'vaga':vaga,
        'related_vagas':rel_vagas,
        'candidatura':candidatura
    })

# Listagem de todas as vagas    
def todas_vagas(request):
    todas_vagas=Vagas.objects.all()
    return render(request,'todas_vagas.html',{
        'todas_vagas':todas_vagas
    })

def todas_categorias(request):
    cats=Categoria.objects.all()
    return render(request,'categorias.html',{
        'cats':cats
    })

# Criado para interagir com categoria_vagas.html
def categoria(request,id):
    categoria=Categoria.objects.get(id=id)
    vagas=Vagas.objects.filter(categoria=categoria)
    return render(request,'categoria_vagas.html',{
        'todas_vagas':vagas,
        'categoria':categoria,
    })