from django.db import models

# Create your models here.

from django.db import models

# Categoria para vagas
class Categoria(models.Model):
    titulo=models.CharField(max_length=200)

    class Meta:
        verbose_name_plural='Categoria'

    def __str__(self):
        return self.titulo

# Modelo de vaga
class Vagas(models.Model):
    categoria=models.ForeignKey(Categoria,on_delete=models.CASCADE)
    titulo=models.CharField(max_length=100)
    resumo=models.TextField()
    detalhe=models.TextField()
    hora_postagem=models.DateTimeField(auto_now_add=True)
    proposta=models.CharField(max_length=100)
    local=models.CharField(max_length=100)
    beneficios=models.CharField(max_length=255)

    class Meta:
        verbose_name_plural='Vagas'

    def __str__(self):
        return self.titulo

# Lanças candidaturas
class Candidatura(models.Model):
    vaga=models.ForeignKey(Vagas,on_delete=models.CASCADE)
    nome=models.CharField(max_length=100)
    email=models.CharField(max_length=200)
    numero=models.TextField()
    escolaridade=models.TextField()
    experiencia=models.TextField()

    status=models.BooleanField(default=False)