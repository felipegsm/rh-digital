
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages
from django.views.decorators.http import require_POST
from django.contrib.auth.models import User

def login_user(request):
    return render(request, 'login.html')

@csrf_protect
def submit_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, 'Usuário e senha inválidos. Tente novamente')
        return redirect('/login/')

def logout_user(request):
    logout(request)
    return redirect('/')


# @require_POST
# def cadastrar_usuario(request):
#     try:
#         usuario_aux = User.objects.get(username=request.POST['username'])

#         if usuario_aux:
#             messages.error(request, 'Usuário e senha inválidos. Tente novamente')

#     except User.DoesNotExist:
#         username = request.POST['username']
#         password = request.POST['password']

#         novoUsuario = User.objects.create_user(username=username, password=password)
#         novoUsuario.save()
        
#         return HttpResponseRedirect('/')


# @require_POST
# def entrar(request):
#     usuario_aux = User.objects.get(email=request.POST['email'])
#     usuario = authenticate(username=usuario_aux.username,
#                            password=request.POST["senha"])
#     if usuario is not None:
#         login(request, usuario)
#         return HttpResponseRedirect('/home/')

#     return HttpResponseRedirect('/')

# @login_required
# def sair(request):
#     logout(request)
#     return HttpResponseRedirect('/')

def cadastro(request):
    return render(request, 'cadastro.html')

@require_POST
def cadastro_usuario(request):
    try:
        usuario_username = User.objects.get(username=request.POST['username'])
        
        if usuario_username:
            messages.error(request, 'Usuário já cadastrado. Tente novamente após alterar os dados de cadastro')
            return render(request, 'cadastro.html')

    except User.DoesNotExist:
        usuario = request.POST.get('username')
        senha = request.POST.get('password')
        novoUsuario = User.objects.create_user(username=usuario, password=senha)
        novoUsuario.save()
        return redirect('/')