from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views
urlpatterns=[
    path('/',views.login_user, name='login'),
    path('/cadastro/',views.cadastro, name='cadastro'),
    path('/cadastro-usuario',views.cadastro_usuario, name='cadastro-usuario'),
    path('/login/submit', views.submit_login, name='login-submit'),
    path('/logout/', views.logout_user, name='logout'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)